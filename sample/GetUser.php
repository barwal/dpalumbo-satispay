<?php

/**
 * @author      Diego Palumbo
 * @version     0.1
 */

require(__DIR__ . "/../entity/UsersApi.php");
require(__DIR__ . "/../client/Web.php");

$id = 'b10da1c6-beff-4401-888b-1c26a55c690a'; //valorizzare con un user id valido
$sample = new UsersApi();
$sample->get($id);

echo "WRAPPER: ";
var_dump($sample);

$client = new Web();
$response = $client->call($sample);

echo "RESPONSE: ";
var_dump($response);
