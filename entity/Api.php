<?php

require_once(__DIR__ . '/../conf/config.php');

class Api
{

    protected $_header = array();
    protected $_content = array();
    protected $_method = '';
    protected $_endpoint = '';

    public function __construct()
    {
        $this->_header = [CONTENT_TYPE, BEARER, ACCEPT];
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function getHeader()
    {
        return $this->_header;
    }

    public function getContent()
    {
        return $this->_content;
    }

    public function getEndpoint()
    {
        return $this->_endpoint;
    }

}
