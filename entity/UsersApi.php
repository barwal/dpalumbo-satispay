<?php

require_once(__DIR__ . '/Api.php');

class UsersApi extends Api
{

    private $_phoneNumber ;

    public function __construct($phoneNumber = null)
    {
        parent::__construct();
        $this->_phoneNumber = $phoneNumber;
    }

    public function post()
    {
        $this->_method = 'POST';
        $this->_endpoint = '/v1/users';
        $this->_content = ['phone_number' => $this->_phoneNumber];
    }

    public function get($id)
    {
        $this->_method = 'GET';
        $this->_endpoint = '/v1/users/'.$id;
    }

    public function userslist()
    {
        $this->_method = 'GET';
        $this->_endpoint = '/v1/users';
    }


}
